import 'babel-polyfill';

import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import Root from './routes';

const root = document.getElementById('root');

render((
  <AppContainer>
    <Root />
  </AppContainer>), root);
