import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { hot } from 'react-hot-loader/root';

import Index from './Index/Index';
import Page2 from './Page2/Index';

class Root extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path='/' component={Index} />
          <Route path='/page2' component={Page2} />
        </div>
      </Router>
    );
  }
}

export default hot(Root);
