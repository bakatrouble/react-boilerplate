'use strict';

const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Path = require('path');
const Webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const pkg = require('./package.json');

const TARGET = process.env.npm_lifecycle_event;
const DEV_SERVER_PORT = process.env.PORT = process.env.PORT || 7000;
const NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV.toLowerCase() : 'development';
const IN_DEV_MODE = NODE_ENV === 'development';

// common plugins
const plugins = [
  new Webpack.DefinePlugin({
    'process.env': {
      // Necessary for applying the correct environment everywhere
      'NODE_ENV': JSON.stringify(NODE_ENV)
    }
  }),
  new HtmlWebpackPlugin({
    filename: 'index.html',
    template: 'src/index.html'
  })
];

// common configuration
const conf = {
  output: {
    path: Path.join(__dirname, 'public'),
    filename: 'assets/[name].js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: ['babel-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.css/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.(png|jpg|gif|woff|svg|eot|ttf|woff2)$/,
        loader: 'url-loader?limit=1024&name=assets/[name]-[hash:8].[ext]!image-webpack-loader?bypassOnDebug'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      'src',
      'node_modules'
    ]
  },
  plugins: plugins,
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          name: 'vendor',
          chunks: 'all',
          test: /[\\/]node_modules[\\/]/
        }
      }
    }
  },
  mode: 'production',
  devServer: {
    contentBase: Path.resolve(__dirname, 'public'),
    historyApiFallback: true,
    hot: true,
    publicPath: '/',
    stats: {
      colors: true,
      chunks: false
    },
    host: '0.0.0.0',
    port: DEV_SERVER_PORT,
    disableHostCheck: true
  }
};

console.log(IN_DEV_MODE);
if (IN_DEV_MODE) { // development mode (webpack-dev-server)
  plugins.push(new Webpack.NoEmitOnErrorsPlugin());
  plugins.push(new Webpack.NamedModulesPlugin());
  plugins.push(new Webpack.HotModuleReplacementPlugin());

  conf.devtool = 'source-map';

  conf.mode = 'development';

  conf.performance = {
    hints: false
  };
  conf.entry = {
    main: [
      Path.join(__dirname, 'src/app')
    ],
    vendor: [
      'react-hot-loader/patch', // this has to be the first loaded plugin in order to work properly!
      'webpack-dev-server/client?http://0.0.0.0:' + DEV_SERVER_PORT, // WebpackDevServer host and port
      'webpack/hot/only-dev-server', // 'only' prevents reload on syntax errors
      // Path.join(__dirname, 'src/app') // appʼs entry point
    ]
  };
} else { // production mode (bundling)
  plugins.push(new UglifyJsPlugin());

  plugins.push(new CleanWebpackPlugin(['public'], {
    root: Path.resolve(__dirname),
    verbose: true,
    dry: false,
    exclude: ['.gitignore']
  }));

  conf.output.filename = 'assets/[name]-[chunkhash].js';
  conf.entry = {
    main: Path.join(__dirname, 'src/app')
  };
}

module.exports = conf;
